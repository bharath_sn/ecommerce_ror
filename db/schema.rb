# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170512022413) do

  create_table "items", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "product"
    t.string "price"
    t.string "description"
    t.string "image_url"
    t.string "collection1"
    t.string "collection2"
    t.string "collection3"
    t.string "collection4"
    t.string "uniqueid"
    t.string "sizes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "karts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "itemid"
    t.string "userid"
    t.string "size"
    t.string "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "itemname"
    t.string "itemimgurl"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "userid"
    t.string "password"
    t.string "firstname"
    t.string "lastname"
    t.string "address"
    t.string "city"
    t.string "zip"
    t.string "state"
    t.string "email"
    t.string "phno"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
