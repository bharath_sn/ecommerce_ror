class KartController < ApplicationController

  def add
    @kart = Kart.new
    @kart.userid = cookies[:user_id]
    @kart.itemid = params[:itemid]
    @kart.itemname = params[:itemname]
    @kart.itemimgurl = params[:itemimgurl]
    @kart.quantity = params[:quantity]
    @kart.size = params[:size]
    @kart.save
    redirect_to "/user/myaccount"
  end

  def remove
      Kart.where("userid" => cookies[:user_id], "itemid" => params[:id]).destroy_all
      redirect_to "/user/myaccount"
  end

end
