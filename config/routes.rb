Rails.application.routes.draw do

  # for items controller
  post 'item/add'
  get 'item/show'
  get 'item/showall'
  post 'item/edit'
  get 'item/index'
  get 'item/delete'

  # for display controller
  get 'display/index' #home page
  get 'display/trousers'
  get 'display/shirt'
  get 'display/tshirt'
  get 'display/sweatshrit'
  get 'display/denim'
  get 'display/boxers'
  get 'display/shorts'
  get 'display/joggers'
  get 'display/jackets'
  get 'display/item'
  get 'display/thebrand'
  get 'display/storelocation'
  get 'display/contact'

  #for display user
  get 'user/login'
  get 'user/registration'
  get 'user/myaccount'
  get 'user/kart'
  post 'user/signup'
  post 'user/signin'
  get 'user/signout'
  get 'user/paymentgateway'

  #for kart management
  post 'kart/add'
  get  'kart/remove'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
