class ItemController < ApplicationController
  def index
  end

  def add
    @uniqueid = ""
    @user = Item.new
    @user.name = params['name']
    @user.product = params['product']
    @user.price = params['price']
    @user.description = params['desc']

    File.open(Rails.root.join('public/itemspic', 'images', params[:name]+"_"+params[:imageurl].original_filename), 'wb') do |f|
      f.write(params[:imageurl].read)
    end

    @user.image_url = "/itemspic/images/"+params[:name]+"_"+params[:imageurl].original_filename
    @user.collection1 = params['collection1']
    @user.collection2 = params['collection2']
    @user.collection3 = params['collection3']
    @user.collection4 = params['collection4']
    @user.sizes = params['sizes']
    @user.uniqueid = rand(36**8).to_s(36)
    @user.save

    @uniqueid = @user.uniqueid
    @user = Item.all
  end

  def showall
      @user = Item.all
      render "showall"
  end

  def edit

  end

  def delete
    Item.where("uniqueid" => params["id"]).destroy_all
    render "index"
  end
end
