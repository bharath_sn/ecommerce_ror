class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :name
      t.string :product
      t.string :price
      t.string :description
      t.string :image_url
      t.string :collection1
      t.string :collection2
      t.string :collection3
      t.string :collection4
      t.string :uniqueid
      t.string :sizes
      t.timestamps
    end
  end
end
