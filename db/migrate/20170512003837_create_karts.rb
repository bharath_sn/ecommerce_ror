class CreateKarts < ActiveRecord::Migration[5.1]
  def change
    create_table :karts do |t|
      t.string :itemid
      t.string :userid
      t.string :size
      t.string :quantity

      t.timestamps
    end
  end
end
